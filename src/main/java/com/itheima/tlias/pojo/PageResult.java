package com.itheima.tlias.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageResult implements Serializable {
    private Long total; // 总记录数
    private List<?> rows; // 当前页数据
}
