package com.itheima.tlias.mapper;

import com.itheima.tlias.pojo.Clazz;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")

public interface ClazzMapper {


    List<Clazz> queryClazz(Clazz clazz);

    List<Clazz> findAll();

    Integer insert(Clazz clazz);

    Clazz findById(Integer id);

    Integer update(Clazz clazz);

    Integer delete(@Param("id") Integer id);

    Integer countStudentsByClazzId(Integer id);
}
