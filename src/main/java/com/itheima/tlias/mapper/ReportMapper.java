package com.itheima.tlias.mapper;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReportMapper {


    @MapKey("job")          //如果查询的记录往Map中封装，可以通过@MapKey注解指定返回的map中的唯一标识是那个字段。【也可以不指定】
    List<Map<String,Object>> getEmpJobData();

    List<Map> getEmpGenderData();
}
