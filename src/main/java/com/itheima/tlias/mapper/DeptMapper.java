package com.itheima.tlias.mapper;

import com.itheima.tlias.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface DeptMapper {
    // 查询所有部门信息
    List<Dept> findAll();
    // 根据id删除部门
    int delete(int id);
    // 根据name添加部门
    int insert(Dept d);

    int update(Dept dept);

    Dept findById(int id);
}
