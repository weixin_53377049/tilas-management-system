package com.itheima.tlias.mapper;

import com.itheima.tlias.pojo.Emp;
import com.itheima.tlias.pojo.EmpExpr;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectKey;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Mapper
public interface EmpMapper {
    List<Emp> findAll();

    List<Emp> findSomeEmp(@Param("emp") Emp emp,@Param("begin") Date begin,@Param("end") Date end);

    int delete(@Param("ids") Integer...ids);
    int deleteEmpExpr(@Param("ids")Integer...ids);

 //@Options(useGeneratedKeys = true,keyProperty = "id")用于获取自增主键
    int insert(@Param("emp") Emp emp);
    int insertEmpExpr(List<EmpExpr> exprList);

    Emp findById(Integer id);
    List<EmpExpr> findEmpExprById(Integer id);

    int update(Emp emp);
}
