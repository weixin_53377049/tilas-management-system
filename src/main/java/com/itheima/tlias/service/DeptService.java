package com.itheima.tlias.service;

import com.itheima.tlias.pojo.Dept;
import com.itheima.tlias.pojo.Result;

import java.time.LocalDateTime;
import java.util.List;

public interface DeptService {
    // 查询所有部门
    List<Dept> findAll();
    // 根据id删除部门
    int delete(int id);
    // 添加部门
    int insert(Dept d);

    int update(Dept dept);

    Dept findById(int id);
}
