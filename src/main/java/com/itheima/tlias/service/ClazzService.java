package com.itheima.tlias.service;

import com.itheima.tlias.pojo.Clazz;
import com.itheima.tlias.pojo.Result;

import java.util.List;

public interface ClazzService {
    List<Clazz> queryClazz(Clazz clazz, int page, int pageSize);

    List<Clazz> findAll();

    Integer insert(Clazz clazz);

    Clazz findById(Integer id);

    Integer update(Clazz clazz);

    Integer delete(Integer id);
}
