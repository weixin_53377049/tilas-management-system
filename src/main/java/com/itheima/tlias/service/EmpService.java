package com.itheima.tlias.service;

import com.itheima.tlias.pojo.Emp;

import java.util.Date;
import java.util.List;

public interface EmpService {
    List<Emp> findAll();

    List<Emp> findSomeEmp(Emp emp, Date begin, Date end, int page, int pageSize);

    int delete(Integer...ids);

    int insert(Emp emp);

    Emp findById(Integer id);

    int update(Emp emp);
}
