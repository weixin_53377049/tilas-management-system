package com.itheima.tlias.service.impl;

import com.itheima.tlias.mapper.ReportMapper;
import com.itheima.tlias.pojo.JobOption;
import com.itheima.tlias.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("ReportServiceImpl")
public class ReportServiceImpl implements ReportService {
    @Autowired
    ReportMapper reportMapper;

    @Override
    public List<Map> getEmpGenderData() {
        return reportMapper.getEmpGenderData();
    }

    @Override
    public JobOption getEmpJobData() {
        List<Map<String, Object>> empJobData = reportMapper.getEmpJobData();
        System.out.println("empJobData = " + empJobData);
        // 将map集合中的job和total分别封装到两个list中,利用stream流代替循环
        List<Object> jobList = empJobData.stream().map(map -> map.get("job")).toList();
        List<Object> dataList = empJobData.stream().map(map -> map.get("total")).toList();
        return new JobOption(jobList, dataList);

    }
}
