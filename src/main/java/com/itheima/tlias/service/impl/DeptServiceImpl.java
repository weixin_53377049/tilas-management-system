package com.itheima.tlias.service.impl;

import com.itheima.tlias.mapper.DeptMapper;
import com.itheima.tlias.pojo.Dept;
import com.itheima.tlias.pojo.Result;
import com.itheima.tlias.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
@Service("DeptServiceImpl")
public class DeptServiceImpl implements DeptService {
    @Resource
    private DeptMapper deptMapper;
    @Override
    public List<Dept> findAll() {
        return deptMapper.findAll();
    }

    @Override
    public int delete(int id) {return deptMapper.delete(id);}

    @Override
    public int insert(Dept d) {
        // 检查 name 是否为空
        //if (d.getName() == null || d.getName().trim().isEmpty()) {
        //    throw new IllegalArgumentException("部门名称不能为空");
        //}
        LocalDateTime dt = LocalDateTime.now();
        d.setCreateTime(dt);
        d.setUpdateTime(dt);
        return deptMapper.insert(d);
    }

    @Override
    public int update(Dept dept) {
        if (dept.getId() >0){
            dept.setUpdateTime(LocalDateTime.now());
            return deptMapper.update(dept);
        }
        return -1;
    }

    @Override
    public Dept findById(int id) {
        if (id > 0){
            return deptMapper.findById(id);
        }
        return null;
    }


}
