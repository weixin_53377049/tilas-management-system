package com.itheima.tlias.service.impl;

import com.github.pagehelper.PageHelper;
import com.itheima.tlias.mapper.ClazzMapper;
import com.itheima.tlias.pojo.Clazz;
import com.itheima.tlias.service.ClazzService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service("ClazzServiceImpl")
@Slf4j
public class ClazzServiceImpl implements ClazzService {
    @Autowired
    ClazzMapper clazzMapper;

    @Override
    public List<Clazz> queryClazz(Clazz clazz, int page, int pageSize) {
        PageHelper.startPage(page, pageSize);
        return clazzMapper.queryClazz(clazz);
    }

    @Override
    public List<Clazz> findAll() {
        return clazzMapper.findAll();
    }

    @Override
    public Integer insert(Clazz clazz) {
        clazz.setCreateTime(LocalDateTime.now());
        clazz.setUpdateTime(LocalDateTime.now());
        return clazzMapper.insert(clazz);
    }

    @Override
    public Clazz findById(Integer id) {

        return clazzMapper.findById(id);
    }

    @Override
    public Integer update(Clazz clazz) {
        clazz.setUpdateTime(LocalDateTime.now());
        return clazzMapper.update(clazz);
    }

    @Override
    public Integer delete(Integer id) {
        int stuClass = clazzMapper.countStudentsByClazzId(id);
        if (stuClass > 0) {
            log.info("该班级有学生，无法删除");
            return 0;
        }
        return clazzMapper.delete(id);
    }
}
