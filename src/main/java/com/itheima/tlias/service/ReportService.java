package com.itheima.tlias.service;

import com.itheima.tlias.pojo.JobOption;

import java.util.List;
import java.util.Map;

public interface ReportService {

    List<Map> getEmpGenderData();

    JobOption getEmpJobData();
}
