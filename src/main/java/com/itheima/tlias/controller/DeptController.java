package com.itheima.tlias.controller;

import com.itheima.tlias.pojo.Dept;
import com.itheima.tlias.pojo.Result;
import com.itheima.tlias.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 部门管理控制器
 */
@RestController
@RequestMapping("/depts")
public class DeptController {
    @Resource()
    private DeptService deptService;

    @GetMapping
    public Result findAll() {
        List<Dept> deptList = deptService.findAll();
        if (deptList != null) {
            return Result.success(deptList);
        }
        return Result.error("查询失败");
    }

    @DeleteMapping
    public Result delete(int id) {
        if (deptService.delete(id) > 0) {
            return Result.success();
        }
        return Result.error("删除失败");
    }


    /*
    接受前端的json类型的参数
        如果你的参数不是key=value的格式，而是key=json串的格式，
        默认SpringMVC采用getParameter()方法将请求体中的参数封装到Map集合中
        但是getPrarameter()方法只能获取key=value的参数，不能获取json类型的参数
        @RequestBody:将请求体中json数据封装到dept对象中
     */
    @PostMapping
    public Result insert(@RequestBody Dept dept){
        if (deptService.insert(dept)> 0) {
            return Result.success();
        }
        return Result.error("添加失败");
    }


    /*
        接受前端的路径参数：
            如果我们没有springMVC对话的话，默认他以为参数是key=valve的底层依然采用的是getParameter方法获取
            我们需要告springMVC我们是路径参数，可以通过@PathVariable
            要求我们的{键}和我们参数名保持一致，才能设置成功
            如果不一致，可以通过value指定

    */

    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") int id){
        Dept dept = deptService.findById(id);
        if (dept != null) {
            return Result.success(dept);
        }
        return Result.error("查询失败");
    }


    @PutMapping
    public Result update(@RequestBody Dept dept){
        if (deptService.update(dept)> 0) {
            return Result.success();
        }
        return Result.error("修改失败");

    }

}
