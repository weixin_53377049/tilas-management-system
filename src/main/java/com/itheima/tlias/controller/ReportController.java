package com.itheima.tlias.controller;

import com.itheima.tlias.pojo.JobOption;
import com.itheima.tlias.pojo.Result;
import com.itheima.tlias.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/report")
public class ReportController {
    @Autowired
    ReportService reportService;

    @RequestMapping("/empJobData")
    public Result getEmpJobData(){
        JobOption empJobData = reportService.getEmpJobData();
        if (empJobData != null) {
            return Result.success(empJobData);
        }
        return Result.error("数据查询失败");
    }
    @RequestMapping("/empGenderData")
    public Object getEmpGenderData(){
        List<Map> empGenderData = reportService.getEmpGenderData();
        if (empGenderData != null)
            return Result.success(empGenderData);
        return Result.error("数据查询失败");
    }
}
