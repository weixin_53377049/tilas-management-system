package com.itheima.tlias;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

@Slf4j
public class LogTest {

    @Test
    public void testLog(){
        Logger logger= LoggerFactory.getLogger("报错啦");
        logger.info("info日志");
        logger.error("error日志");
        logger.debug("debug日志");
        logger.warn("warn日志");

        log.info("a={},b={}",3,4,10,20);
    }

}
